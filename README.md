This was built for a ras pi zero, look at some of the other projects I have for newer ras pi's.

The zero is armv6 architecture and needs a different compiler

https://github.com/raspberrypi/tools/tree/master/arm-bcm2708

I followed the symlink in that dir to choose the right compiler

Create `~/.cargo/config`

```
[target.arm-unknown-linux-gnueabihf]
linker = "/home/user/cc/tools-master/arm-bcm2708/arm-rpi-4.9.3-linux-gnueabihf/bin/arm-linux-gnueabihf-gcc"
strip = { path="/home/user/cc/tools-master/arm-bcm2708/arm-rpi-4.9.3-linux-gnueabihf/bin/arm-linux-gnueabihf-strip" }
```

Note: the path to strip is necessary for cargo-deb

Add the target to rust

```
rustup override set nightly
rustup target add arm-unknown-linux-gnueabihf
```

Install cargo-deb

```
cargo install cargo-deb
```

I had zero luck trying to cross compile the mosquitto library, there is some support for this, however it would also require cross compiling the necessary deps like openssl, which is possible, however I see no where in the Make config for mosquitto to tell it how to use your cross compiled deps.

In the end I said fuck it and just pulled the pre-compiled lib and deps from debian by adding the armhf arch to my build machine and doing apt download with a dpkg -x to get all the necessary so's

You then need to tell cargo how to tell rust how to find them, this is done in the `build.rs` script

```
sudo dpkg --add-architecture armhf
sudo apt update
apt download libmosquitto-dev:armhf
apt download libmosquitto1:armhf
apt download libc-ares2:armhf
apt download libc-ares-dev:armhf
apt download libssl1.1:armhf
apt download libssl-dev:armhf
dpkg -x *.deb .
```

Make sure you have the proper cross compiler we found above on the path, needed for things with native deps that are built on the fly like ring

```shell
export PATH=$PATH:/home/ed/cc/tools-master/arm-bcm2708/arm-rpi-4.9.3-linux-gnueabihf/bin/
```

You should now be able to build for the raspberry pi like so:

```
cargo build --target=arm-unknown-linux-gnueabihf
```

If you want to make a deb:

```
cargo deb --target=arm-unknown-linux-gnueabihf
```

If you don't want to cross compile at all, you can probably just `apt install` all those deps and just run `cargo build` without specifying a target.

If you are trying to build this for another architecture, you will need to update build.rs with additional entries for your arch as well as provide the proper cross compiled shared objects for linking.

Or try your hand at one of the pure rust mqtt implementations, I was not impressed by any however, most don't seem to have any active support. :(

## Ras Pi Setup

```
sudo apt install libmosquitto-dev
```