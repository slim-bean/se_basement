#!/bin/bash -e

HOST=$1

cargo deb --target=arm-unknown-linux-gnueabihf
scp target/arm-unknown-linux-gnueabihf/debian/se_basement_0.1.0_armhf.deb  pi@${HOST}:
ssh pi@${HOST} "sudo dpkg -i se_basement_0.1.0_armhf.deb"