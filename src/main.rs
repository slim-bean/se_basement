#![feature(duration_as_u128)]

#[macro_use]
extern crate log;
extern crate log4rs;
extern crate chrono;

extern crate embedded_hal as hal;
extern crate linux_embedded_hal as linux_hal;

extern crate mosquitto_client;
extern crate serde_derive;
extern crate serde_yaml;
extern crate serde_json;
extern crate dtoa;

extern crate htu21d;
extern crate sensor_lib;

use linux_hal::sysfs_gpio::{Direction, Edge};
use linux_hal::sysfs_gpio::Pin as Sysfs_Pin;

use linux_hal::{I2cdev, Delay};
use htu21d::HTU21D;

use std::thread;
use std::time::{Duration};
use std::sync::mpsc;
use std::sync::Arc;
use std::time::SystemTime;
use std::path::Path;


extern crate serial;



use sensor_lib::{SensorValue, TempHumidityValue, load_from_file};

mod omnimeter;
use omnimeter::Omnimeter;

pub struct Payload {
    queue: String,
    bytes: String
}

fn main() {
    //Check if there is a logging config configured in /etc if not just use one local to the app
    let etc_config = Path::new("/etc/se_basement/log4rs.yml");
    let log_config_path = if let true = etc_config.exists() {
        etc_config
    } else {
        Path::new("log4rs.yml")
    };
    log4rs::init_file(log_config_path, Default::default()).expect("Failed to init logger");
    info!("Starting se_basement");

    info!("Configuring the receive interrupt");
    let interrupt = Sysfs_Pin::new(22);
    interrupt.export().unwrap();
    interrupt.set_direction(Direction::In).unwrap();
    interrupt.set_edge(Edge::FallingEdge).unwrap();
    let mut poller = interrupt.get_poller().unwrap();

    info!("Loading sensors YAML file");
    let sensors = load_from_file("/etc/se_basement/sensors.yml");
    let thread1_sensors = Arc::new(sensors);
    let thread2_sensors = Arc::clone(&thread1_sensors);

    let (tx, rx) = mpsc::channel::<Payload>();
    let tx1 = mpsc::Sender::clone(&tx);
    let tx2 = mpsc::Sender::clone(&tx);

    info!("Opening I2C device");
    let dev = I2cdev::new("/dev/i2c-1").unwrap();

    info!("Create and reset HTU21D");
    let mut htu21d = HTU21D::new(dev, Delay);
    htu21d.reset().unwrap();

    info!("Creating Omnimeter");
    let omni = Omnimeter::new(tx2, "/dev/serial0").expect("Failed to init Omnimeter");


    info!("Connecting to MQ Broker");
    let m = mosquitto_client::Mosquitto::new("se_monitor");
    m.connect_wait("mq.edjusted.com", 1883, 5000).expect("Failed to connect");
    info!("Connected to MQ Broker");

    info!("Setting up threads");


    thread::spawn(move || {
        let mut dwellCounter = 0;
        loop {
            //Poll every 500ms looking for low state
            thread::sleep(Duration::from_millis(500));

            match interrupt.get_value() {
                Ok(val) => {
                    //Pulse is active low, ignore all high pulses and keep dwellCounter at 0
                    if val == 1 {
                        dwellCounter = 0;
                        continue;
                    }
                    //Only send a message if the pin stays low for 1.5s (3x poll interval)
                    //If pin continues to stay low dwellCounter will continue to increment and we won't send duplicate messages
                    if val == 0 && dwellCounter == 2 {
                        info!("1 cubic foot of propane has been consumed: current_val {}", val);
                        let temp_val = SensorValue {
                            id: 100,
                            timestamp: SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap().as_millis() as u64,
                            value: String::from("1"),
                        };

                        for queue in &thread1_sensors.get(&100i16).unwrap().destination_queues {
                            match serde_json::to_string(&temp_val) {
                                Ok(val) => {
                                    tx.send(Payload {
                                        queue: queue.to_string(),
                                        bytes: val,
                                    }).unwrap();
                                },
                                Err(err) => {
                                    error!("Failed to serialize the sensor value: {}", err);
                                },
                            }
                        }
                    }
                    dwellCounter = dwellCounter + 1;
                },
                Err(err) => {
                    warn!("Failed to read propane pin: {}", err);
                },
            }
        }
    });

    thread::spawn(move || {
        loop {
            let temp = (htu21d.read_temperature().unwrap() * 1.8) + 32f32;
            let humidity = htu21d.read_humidity().unwrap();

            let mut buf = [b'\0'; 30];
            let len = dtoa::write(&mut buf[..], temp).unwrap();
            let flt_as_string = std::str::from_utf8(&buf[..len]).unwrap();

            let temp_val = SensorValue{
                id: 101,
                timestamp: SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap().as_millis() as u64,
                value: String::from(flt_as_string),
            };

            for queue in &thread2_sensors.get(&101i16).unwrap().destination_queues {
                match serde_json::to_string(&temp_val) {
                    Ok(val) => {
                        tx1.send(Payload {
                            queue: queue.to_string(),
                            bytes: val,
                        }).unwrap();
                    },
                    Err(err) => {
                        error!("Failed to serialize the sensor value: {}", err);
                    },
                }

            }

            let mut buf = [b'\0'; 30];
            let len = dtoa::write(&mut buf[..], humidity).unwrap();
            let flt_as_string = std::str::from_utf8(&buf[..len]).unwrap();

            let temp_val = SensorValue{
                id: 102,
                timestamp: SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap().as_millis() as u64,
                value: String::from(flt_as_string),
            };

            for queue in &thread2_sensors.get(&102i16).unwrap().destination_queues {
                match serde_json::to_string(&temp_val) {
                    Ok(val) => {
                        tx1.send(Payload {
                            queue: queue.to_string(),
                            bytes: val,
                        }).unwrap();
                    },
                    Err(err) => {
                        error!("Failed to serialize the sensor value: {}", err);
                    },
                }

            }

            let temp_humidity = TempHumidityValue{
                timestamp: SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap().as_millis() as u64,
                location: 3,
                temp: temp,
                humidity: humidity,
            };

            match serde_json::to_string(&temp_humidity) {
                Ok(val) => {
                    tx1.send(Payload {
                        queue: String::from("/ws/3/grp/temp_humidity"),
                        bytes: val,
                    }).unwrap();
                },
                Err(err) => {
                    error!("Failed to serialize the sensor value: {}", err);
                },
            }

            info!("Temp: {}, Humidity: {}", temp, humidity);
            thread::sleep(Duration::from_secs(60));
        }
    });

    Omnimeter::start_thread(omni);

    info!("Running");

    for received in rx {
        for i in 1..6 {
            match m.publish_wait(&received.queue, received.bytes.as_bytes(), 2, false, 1000) {
                Ok(id) => {
                    debug!("Message {} published succesfully after {} attempts", id, i);
                    break;
                }
                Err(e) => {
                    // Ignore the first retry because it's noisy, start warning if we fail more than once.
                    if i > 1 {
                        warn!("Failed to enqueue data: {}, will retry {} more times", e, 5-i);
                    }
                    if i == 5 {
                        error!("Failed to enqueue message after 5 tries, it will be dropped");
                        panic!("Failed to enqueue message after 5 tries, it will be dropped");
                    }
                }
            };
        }

    }

}
